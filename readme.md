### A simple library for creating self documenting tests.

  #### A first feature can be created that uses scenario state

  * < 1ms given I have a first feature where I set a message ✔ 

  * < 1ms when I update the text in the state property ✔ 

  * < 1ms then the value stored is updated ✔ 

    #### A child scenario of Scenario 1 can be created

    * < 1ms given that I have created a child feature of Scenario 1 ✔ 

    * < 1ms when the test runs ✔ 

  #### A first feature can be created that uses scenario state

  #### I can pass initial state params and modify and use them

  * < 1ms given that the state is set to it's initial values ✔ 

  * < 1ms when I change the values ✔ 
### A simple library for creating self documenting tests.

### This is a broken feature

  #### This is the failing scenario

  * < 1ms given that I have a feature that is broken ✔ 

  * < 1ms when when the failing scenario is tested ✔ 
### This is a broken feature

### Stand alone assertion tests (unit testing) can be executed for a feature.
### 1ms assert True is equal to true ✔ 
### < 1ms assert 12 - 4 = 8 ✔ 
### < 1ms assert The first 4 letters of heroic are hero ✔ 

  #### Stand alone assertion tests (unit testing) can be executed for a scenario.

  * < 1ms assert False is not equal to true ✔ 

  * < 1ms assert 12 - 8 = 4 ✔ 

  * < 1ms assert the first 4 letters of banter are bant ✔ 

  * assert this is an assertion that is left pendingpending"pending"

  * < 1ms assert this is an assertion that should also be tested despite the previous one being pending ✔ 

  * assert this is an assertion that fails ✘ "failed"

  * < 1ms assert this is an assertion that should pass despite the previous one failing ✔ 

  * 534ms assert this test takes less than a second ✔ 
### Stand alone assertion tests (unit testing) can be executed for a feature.

### Pending "given" and "when" tests stop tests until the next scenario starts

  #### A scenario starts

  * given This one is pendingpending"pending"

  * when this is run it is pending too, despite returning truepending"pending"

  * then this is also pending despite returning falsepending"pending"
### Pending "given" and "when" tests stop tests until the next scenario starts
