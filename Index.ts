import Chalk from 'chalk';

interface IAssert<TParentStateType> {
    scenario: <TStateType>(this: void, description: string, initialState?: TStateType) => IScenario<TParentStateType, TStateType>;
    assert: (this: {}, description: string, step?: Step<TParentStateType>) => IAssert<TParentStateType>;
}

interface IAnd<TStepType, TStateType> {
    and: (this: void, description: string, step?: Step<TStateType>) => TStepType;
}

interface IGiven<TParentStateType, TStateType> extends IAnd<IGiven<TParentStateType, TStateType>, TStateType> {
    when: (this: {}, description: string, step?: Step<TStateType>) => IWhen<TParentStateType, TStateType>;
}

interface IWhen<TParentStateType, TStateType> extends IAnd<IWhen<TParentStateType, TStateType>, TStateType> {
    then: (this: void, description: string, step?: Step<TStateType>) => IThen<TParentStateType, TStateType>;
}

interface IThen<TParentStateType, TStateType> extends IAnd<IThen<TParentStateType, TStateType>, TStateType> {
    finish: (this: void, teardown?: Step<TStateType>) => IFeature<TParentStateType>;
}

export interface IFeature<TFeatureStateType> {
    assert: (this: {}, description: string, assertion?: Step<TFeatureStateType>) => IAssert<TFeatureStateType>;
    scenario: <TStateType>(this: void, description: string, initialState?: TStateType) => IScenario<TFeatureStateType, TStateType>;
    state: TFeatureStateType;
    run: (log?: LogTypes) => Promise<Array<TestResult>>;
    reset: () => void;
    runMarkdown: () => Promise<Array<TestResult>>;
    runHtml: () => Promise<Array<TestResult>>;
}

interface IScenario<TParentStateType, TStateType> {
    scenario: <TChildStateType>(this: void, description: string, initialState?: TChildStateType) => IScenario<TStateType, TChildStateType>;
    assert: (this: {}, description: string, assertion?: Step<TStateType>) => IAssert<TParentStateType>;
    given: (this: {}, description: string, step?: Step<TStateType>) => IGiven<TParentStateType, TStateType>;
    state: Array<TStateType>;
}

interface ISequenceItem {
    verb: Verb;
    description?: string;
    step: Step<any> | Test | undefined;
}

export type LogTypes = 'all' | 'summary' | 'fails';

export type ResultType = 'step' | 'header' | 'footer';

export type Verb = 'given' | 'when' | 'then' | 'and' | 'finish' | 'sub' | 'assert';

export type Step<TStateType> = string | boolean | void | undefined | ((this: void, state: TStateType) => void | boolean | PromiseLike<boolean | void>);

export class TestResult {
    private constructor(
        public description: string,
        public type: ResultType,
        public resultData: any,
        public level: number,
        public success?: boolean,
        public startMs?: number
    ) {
        if (startMs) {
            const time = this.msToTime(new Date().getTime() - startMs);
            this.description = type === 'step' && success ? `${time}${this.description}` : this.description;
        }
    }
    public static pass(startTime: number, description: string, type: ResultType, level: number, result?: any) {
        return new TestResult(description, type, result, level, true, startTime);
    }
    public static fail(startTime: number, description: string, type: ResultType, level: number, result?: any) {
        return new TestResult(description, type, result, level, false, startTime);
    }
    public static pending(description: string, type: ResultType, level: number) {
        return new TestResult(description, type, 'pending', level);
    }

    public get isStep() {
        return this.type === 'step';
    }

    private msToTime(totalMs: number) {
        if (totalMs === 0) return '< 1ms ';

        const d = Math.floor(totalMs / (3600000 * 20));
        const days = d ? d + 'd ' : '';

        const h = Math.floor(totalMs / 3600000) % 24;
        const hours = !!d || h ? h + 'h ' : '';

        const m = Math.floor(totalMs / 60000) % 60;
        const minutes = !!d || (!!h && m) ? m + 'm ' : '';

        let secondsOrMilliseconds: string = '';
        if (totalMs < 1000) secondsOrMilliseconds = totalMs + 'ms';
        else if (totalMs < 3000) secondsOrMilliseconds = (totalMs / 1000).toFixed(1) + 's';
        else if (h < 1) secondsOrMilliseconds = Math.floor(totalMs / 1000) + 's';

        return `${days}${hours}${minutes}${secondsOrMilliseconds} `;
    }
}

class Test {
    private _hideTimes = false;
    private sequence: Array<ISequenceItem> = [];
    public constructor(private readonly description: string, state?: any) {
        this.state = state || {};
    }
    private state: any;

    public describe() {
        /* tslint:disable */ console.log(JSON.stringify(this, null, 4)); /* tslint:enable */
    }

    private compileSteps(level: number = 0, testResults: Array<TestResult> = [], log: LogTypes | null = null): Promise<TestResult> {
        let footer = TestResult.pass(0, this.description, 'footer', level);
        let header = TestResult.pending(this.description, 'header', level);
        let chain = Promise.resolve(header);
        for (let sequenceItem of this.sequence) {
            chain = chain.then(testResult => {
                if (testResult) {
                    testResults.push(testResult);
                    if (
                        (log === 'all' && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined))) ||
                        (testResult.type === 'footer' && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined))
                            && log === 'summary') ||
                        (testResult.type !== 'header' && (log === 'fails' || log === 'summary')
                            && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined)))
                    ) {
                        this.logTestResult(testResult);
                    }
                }
                if (
                    typeof sequenceItem.step === 'undefined' ||
                    (testResult.isStep && testResult.success !== true && sequenceItem.verb
                        && sequenceItem.verb !== 'assert' && sequenceItem.verb !== 'sub')
                ) {
                    let desc = this.getSequenceDescription(sequenceItem) || sequenceItem.verb;
                    delete footer.success;
                    return Promise.resolve(TestResult.pending(desc, 'step', level));
                } else if (sequenceItem.verb === 'sub') {
                    return (<Test>sequenceItem.step).compileSteps(level + 1, testResults, log);
                }
                return this.getStepPromise(footer, sequenceItem, level);
            });
        }

        chain = chain.then(testResult => {
            if (
                log === 'all' ||
                (testResult.type === 'footer' && log === 'summary') ||
                (testResult.type !== 'header' && (log === 'fails' || log === 'summary') && testResult.success !== true)
            ) {
                this.logTestResult(testResult);
            }
            return Promise.resolve(footer);
        });
        return chain;
    }

    private logTestResult(r: TestResult) {
        /* tslint:disable */
        let successMark = r.isStep ? (typeof r.success === 'undefined' ? Chalk.blue.bold('? ') : r.success ? Chalk.green('✔ ') : Chalk.red('✘ ')) : '';
        let description = r.isStep
            ? Chalk.reset(r.description)
            : r.success === true ? Chalk.green(r.description) : r.success === false ? Chalk.red(r.description) : Chalk.blue.bold(r.description);
        let info = r.isStep ? this.resultDetails(r.resultData) : '';
        let line = successMark + description + ' ' + info + (r.type !== 'step' ? '\n' : '');
        console.log(line);
        /* tslint:enable */
    }

    private getStepPromise(parent: TestResult, sequenceItem: ISequenceItem, level: number) {
        return new Promise<TestResult>(resolve => {
            let start: number | undefined;
            if (!this._hideTimes) start = new Date().getTime();
            try {
                const step = <Step<any>>sequenceItem.step;
                if (typeof step === 'function') {
                    let result: any = step(this.state);
                    if (typeof result === 'undefined' || typeof result === 'boolean') {
                        resolve(this.getTestResult(result, sequenceItem, parent, level, start));
                    } else if (result.then && result.catch) {
                        (<Promise<boolean>>result)
                            .then(deferredResult => {
                                return resolve(this.getTestResult(deferredResult, sequenceItem, parent, level, start));
                            })
                            .catch(error => {
                                parent.success = false;
                                return resolve(TestResult.fail(start || 0, this.getSequenceDescription(sequenceItem), 'step', level, error));
                            });
                    } else {
                        resolve(TestResult.pass(start || 0, this.getSequenceDescription(sequenceItem), 'step', level));
                    }
                } else if (typeof step === 'string' || typeof step === 'boolean') {
                    return resolve(this.getTestResult(step, sequenceItem, parent, level, start));
                }
            } catch (error) {
                parent.success = false;
                resolve(TestResult.fail(start || 0, this.getSequenceDescription(sequenceItem), 'step', level, error));
            }
        });
    }

    private getSequenceDescription(sequenceItem: ISequenceItem) {
        return (sequenceItem.verb === 'sub' ? '' : sequenceItem.verb + ' ') + sequenceItem.description;
    }

    private getTestResult(result: any, sequenceItem: ISequenceItem, parent: TestResult, level: number, startTime?: number) {
        if (typeof result === 'undefined') result = true;
        if (!result) {
            parent.success = false;
            return TestResult.fail(startTime || 0, this.getSequenceDescription(sequenceItem), 'step', level, 'failed');
        }

        return TestResult.pass(startTime || 0, this.getSequenceDescription(sequenceItem) + (typeof result === 'string' ? ` (${result})` : ''), 'step', level);
    }

    private resultDetails(object: any): string {
        try {
            if (object && object.message) return JSON.stringify(object.message);
            return object ? JSON.stringify(object, null, 4) : '';
        } catch (error) {
            return 'Object';
        }
    }

    public runMarkdown(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.run()
                .then(results => {
                    let output = '';
                    results.forEach(r => {
                        let description = r.description.replace(/\n/g, '\n\n');
                        /* tslint:disable */
                        let info = r.isStep
                            ? (typeof r.success === 'undefined' ? 'pending' : r.success ? ' ✔ ' : ' ✘ ') + this.resultDetails(r.resultData)
                            : '';
                        /* tslint:enable */
                        output += (r.level === 0 ? '### ' : ('\n' + Array(r.level + 1).join('  ') + (r.isStep ? '* ' : '#### '))) + description + info + '\n';
                    });
                    resolve(output);
                })
                .catch(reject);
        });
    }

    public runHtml(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.run()
                .then(results => {
                    let output = '<div id="results">\n';
                    let inList = false;
                    results.forEach(r => {
                        let description = r.description.replace(/\n/g, '<br>\n');
                        /* tslint:disable */
                        let info = r.isStep
                            ? (typeof r.success === 'undefined' ? 'pending' : r.success ? ' ✔ ' : ' ✘ ') + this.resultDetails(r.resultData)
                            : '';
                        /* tslint:enable */
                        if (!inList && r.isStep) {
                            output += Array(r.level + 1).join('    ') + '<ul>\n';
                            inList = true;
                        }

                        if (r.isStep) {
                            output += Array(r.level + 1).join('    ');
                            output += '    <li class="level_' + (r.level + 1) + '">' + description + info + '</li>\n';
                        }

                        if (inList && (!r.isStep || results.indexOf(r) === results.length - 1)) {
                            output += Array(r.level + 1).join('    ') + '</ul>\n';
                            inList = false;
                        }

                        if (!r.isStep) {
                            output += Array(r.level + 1).join('    ');
                            output += '<h' + (r.level + 1) + '>' + description + info + '</h' + (r.level + 1) + '>\n';
                        }
                    });
                    output += '</div>';
                    resolve(output);
                })
                .catch(reject);
        });
    }

    _results: Array<TestResult> | null;
    public reset() {
        this._results = null;
    }
    public run(log?: LogTypes, hideTimes = false): Promise<Array<TestResult>> {
        this._hideTimes = hideTimes;
        return new Promise<Array<TestResult>>((resolve, reject) => {
            if (this._results) return resolve(this._results);
            let results: Array<TestResult> = [];
            this.compileSteps(0, results, log)
                .then(lastResult => {
                    results.push(lastResult);
                    this._results = results.filter(result => !!result);
                    resolve(this._results);
                })
                .catch(reject);
        });
    }

    public scenario(description: string, initialState: any) {
        let subTest = new Test(description, initialState);
        this.sequence.push({ verb: 'sub', step: subTest });
        return subTest;
    }
    private step(verb: Verb, description?: string, step?: Step<any>): Test {
        this.sequence.push({ verb, description, step: step });
        return this;
    }
    public given = (description: string, step?: Step<any>) => this.step('given', description, step);
    public when = (description: string, step?: Step<any>) => this.step('when', description, step);
    public then = (description: string, step?: Step<any>) => this.step('then', description, step);
    public and = (description: string, step?: Step<any>) => this.step('and', description, step);
    public finish = (teardown: Step<any>) => this.step('finish', undefined, teardown);
    public assert = (description: string, step?: Step<any>) => this.step('assert', description, step);
}

export class Feature {
    public static start<TFeatureStateType>(description: string): IFeature<TFeatureStateType> {
        return <any>new Test(description, 'feature');
    }
}
