"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chalk_1 = require("chalk");
class TestResult {
    constructor(description, type, resultData, level, success, startMs) {
        this.description = description;
        this.type = type;
        this.resultData = resultData;
        this.level = level;
        this.success = success;
        this.startMs = startMs;
        if (startMs) {
            const time = this.msToTime(new Date().getTime() - startMs);
            this.description = type === 'step' && success ? `${time}${this.description}` : this.description;
        }
    }
    static pass(startTime, description, type, level, result) {
        return new TestResult(description, type, result, level, true, startTime);
    }
    static fail(startTime, description, type, level, result) {
        return new TestResult(description, type, result, level, false, startTime);
    }
    static pending(description, type, level) {
        return new TestResult(description, type, 'pending', level);
    }
    get isStep() {
        return this.type === 'step';
    }
    msToTime(totalMs) {
        if (totalMs === 0)
            return '< 1ms ';
        const d = Math.floor(totalMs / (3600000 * 20));
        const days = d ? d + 'd ' : '';
        const h = Math.floor(totalMs / 3600000) % 24;
        const hours = !!d || h ? h + 'h ' : '';
        const m = Math.floor(totalMs / 60000) % 60;
        const minutes = !!d || (!!h && m) ? m + 'm ' : '';
        let secondsOrMilliseconds = '';
        if (totalMs < 1000)
            secondsOrMilliseconds = totalMs + 'ms';
        else if (totalMs < 3000)
            secondsOrMilliseconds = (totalMs / 1000).toFixed(1) + 's';
        else if (h < 1)
            secondsOrMilliseconds = Math.floor(totalMs / 1000) + 's';
        return `${days}${hours}${minutes}${secondsOrMilliseconds} `;
    }
}
exports.TestResult = TestResult;
class Test {
    constructor(description, state) {
        this.description = description;
        this._hideTimes = false;
        this.sequence = [];
        this.given = (description, step) => this.step('given', description, step);
        this.when = (description, step) => this.step('when', description, step);
        this.then = (description, step) => this.step('then', description, step);
        this.and = (description, step) => this.step('and', description, step);
        this.finish = (teardown) => this.step('finish', undefined, teardown);
        this.assert = (description, step) => this.step('assert', description, step);
        this.state = state || {};
    }
    describe() {
        /* tslint:disable */ console.log(JSON.stringify(this, null, 4)); /* tslint:enable */
    }
    compileSteps(level = 0, testResults = [], log = null) {
        let footer = TestResult.pass(0, this.description, 'footer', level);
        let header = TestResult.pending(this.description, 'header', level);
        let chain = Promise.resolve(header);
        for (let sequenceItem of this.sequence) {
            chain = chain.then(testResult => {
                if (testResult) {
                    testResults.push(testResult);
                    if ((log === 'all' && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined))) ||
                        (testResult.type === 'footer' && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined))
                            && log === 'summary') ||
                        (testResult.type !== 'header' && (log === 'fails' || log === 'summary')
                            && (testResult.type !== 'footer' || (testResult.success !== true && testResult.success !== undefined)))) {
                        this.logTestResult(testResult);
                    }
                }
                if (typeof sequenceItem.step === 'undefined' ||
                    (testResult.isStep && testResult.success !== true && sequenceItem.verb
                        && sequenceItem.verb !== 'assert' && sequenceItem.verb !== 'sub')) {
                    let desc = this.getSequenceDescription(sequenceItem) || sequenceItem.verb;
                    delete footer.success;
                    return Promise.resolve(TestResult.pending(desc, 'step', level));
                }
                else if (sequenceItem.verb === 'sub') {
                    return sequenceItem.step.compileSteps(level + 1, testResults, log);
                }
                return this.getStepPromise(footer, sequenceItem, level);
            });
        }
        chain = chain.then(testResult => {
            if (log === 'all' ||
                (testResult.type === 'footer' && log === 'summary') ||
                (testResult.type !== 'header' && (log === 'fails' || log === 'summary') && testResult.success !== true)) {
                this.logTestResult(testResult);
            }
            return Promise.resolve(footer);
        });
        return chain;
    }
    logTestResult(r) {
        /* tslint:disable */
        let successMark = r.isStep ? (typeof r.success === 'undefined' ? chalk_1.default.blue.bold('? ') : r.success ? chalk_1.default.green('✔ ') : chalk_1.default.red('✘ ')) : '';
        let description = r.isStep
            ? chalk_1.default.reset(r.description)
            : r.success === true ? chalk_1.default.green(r.description) : r.success === false ? chalk_1.default.red(r.description) : chalk_1.default.blue.bold(r.description);
        let info = r.isStep ? this.resultDetails(r.resultData) : '';
        let line = successMark + description + ' ' + info + (r.type !== 'step' ? '\n' : '');
        console.log(line);
        /* tslint:enable */
    }
    getStepPromise(parent, sequenceItem, level) {
        return new Promise(resolve => {
            let start;
            if (!this._hideTimes)
                start = new Date().getTime();
            try {
                const step = sequenceItem.step;
                if (typeof step === 'function') {
                    let result = step(this.state);
                    if (typeof result === 'undefined' || typeof result === 'boolean') {
                        resolve(this.getTestResult(result, sequenceItem, parent, level, start));
                    }
                    else if (result.then && result.catch) {
                        result
                            .then(deferredResult => {
                            return resolve(this.getTestResult(deferredResult, sequenceItem, parent, level, start));
                        })
                            .catch(error => {
                            parent.success = false;
                            return resolve(TestResult.fail(start || 0, this.getSequenceDescription(sequenceItem), 'step', level, error));
                        });
                    }
                    else {
                        resolve(TestResult.pass(start || 0, this.getSequenceDescription(sequenceItem), 'step', level));
                    }
                }
                else if (typeof step === 'string' || typeof step === 'boolean') {
                    return resolve(this.getTestResult(step, sequenceItem, parent, level, start));
                }
            }
            catch (error) {
                parent.success = false;
                resolve(TestResult.fail(start || 0, this.getSequenceDescription(sequenceItem), 'step', level, error));
            }
        });
    }
    getSequenceDescription(sequenceItem) {
        return (sequenceItem.verb === 'sub' ? '' : sequenceItem.verb + ' ') + sequenceItem.description;
    }
    getTestResult(result, sequenceItem, parent, level, startTime) {
        if (typeof result === 'undefined')
            result = true;
        if (!result) {
            parent.success = false;
            return TestResult.fail(startTime || 0, this.getSequenceDescription(sequenceItem), 'step', level, 'failed');
        }
        return TestResult.pass(startTime || 0, this.getSequenceDescription(sequenceItem) + (typeof result === 'string' ? ` (${result})` : ''), 'step', level);
    }
    resultDetails(object) {
        try {
            if (object && object.message)
                return JSON.stringify(object.message);
            return object ? JSON.stringify(object, null, 4) : '';
        }
        catch (error) {
            return 'Object';
        }
    }
    runMarkdown() {
        return new Promise((resolve, reject) => {
            this.run()
                .then(results => {
                let output = '';
                results.forEach(r => {
                    let description = r.description.replace(/\n/g, '\n\n');
                    /* tslint:disable */
                    let info = r.isStep
                        ? (typeof r.success === 'undefined' ? 'pending' : r.success ? ' ✔ ' : ' ✘ ') + this.resultDetails(r.resultData)
                        : '';
                    /* tslint:enable */
                    output += (r.level === 0 ? '### ' : ('\n' + Array(r.level + 1).join('  ') + (r.isStep ? '* ' : '#### '))) + description + info + '\n';
                });
                resolve(output);
            })
                .catch(reject);
        });
    }
    runHtml() {
        return new Promise((resolve, reject) => {
            this.run()
                .then(results => {
                let output = '<div id="results">\n';
                let inList = false;
                results.forEach(r => {
                    let description = r.description.replace(/\n/g, '<br>\n');
                    /* tslint:disable */
                    let info = r.isStep
                        ? (typeof r.success === 'undefined' ? 'pending' : r.success ? ' ✔ ' : ' ✘ ') + this.resultDetails(r.resultData)
                        : '';
                    /* tslint:enable */
                    if (!inList && r.isStep) {
                        output += Array(r.level + 1).join('    ') + '<ul>\n';
                        inList = true;
                    }
                    if (r.isStep) {
                        output += Array(r.level + 1).join('    ');
                        output += '    <li class="level_' + (r.level + 1) + '">' + description + info + '</li>\n';
                    }
                    if (inList && (!r.isStep || results.indexOf(r) === results.length - 1)) {
                        output += Array(r.level + 1).join('    ') + '</ul>\n';
                        inList = false;
                    }
                    if (!r.isStep) {
                        output += Array(r.level + 1).join('    ');
                        output += '<h' + (r.level + 1) + '>' + description + info + '</h' + (r.level + 1) + '>\n';
                    }
                });
                output += '</div>';
                resolve(output);
            })
                .catch(reject);
        });
    }
    reset() {
        this._results = null;
    }
    run(log, hideTimes = false) {
        this._hideTimes = hideTimes;
        return new Promise((resolve, reject) => {
            if (this._results)
                return resolve(this._results);
            let results = [];
            this.compileSteps(0, results, log)
                .then(lastResult => {
                results.push(lastResult);
                this._results = results.filter(result => !!result);
                resolve(this._results);
            })
                .catch(reject);
        });
    }
    scenario(description, initialState) {
        let subTest = new Test(description, initialState);
        this.sequence.push({ verb: 'sub', step: subTest });
        return subTest;
    }
    step(verb, description, step) {
        this.sequence.push({ verb, description, step: step });
        return this;
    }
}
class Feature {
    static start(description) {
        return new Test(description, 'feature');
    }
}
exports.Feature = Feature;
//# sourceMappingURL=Index.js.map