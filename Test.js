"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const _1 = require(".");
let workingFeature = _1.Feature.start(`A simple library for creating self documenting tests.`);
let assertFeature = _1.Feature.start('Stand alone assertion tests (unit testing) can be executed for a feature.');
assertFeature
    .assert('True is equal to true', () => true)
    .assert('12 - 4 = 8', () => 12 - 4 === 8)
    .assert('The first 4 letters of heroic are hero', () => 'heroic'.substring(0, 4) === 'hero')
    .scenario('Stand alone assertion tests (unit testing) can be executed for a scenario.')
    .assert('False is not equal to true', () => !false)
    .assert('12 - 8 = 4', () => 12 - 8 === 4)
    .assert('the first 4 letters of banter are bant', () => 'banter'.substring(0, 4) === 'bant')
    .assert('this is an assertion that is left pending')
    .assert('this is an assertion that should also be tested despite the previous one being pending', () => true)
    .assert('this is an assertion that fails', () => false)
    .assert('this is an assertion that should pass despite the previous one failing', () => true)
    .assert('this test takes less than a second', () => new Promise(resolve => setTimeout(() => resolve(true), 532)))
    .assert('this test takes more than a second but less than 10 seconds', () => new Promise(resolve => setTimeout(() => resolve(true), 1132)));
let pendingFeature = _1.Feature.start('Pending "given" and "when" tests stop tests until the next scenario starts');
pendingFeature.scenario('A scenario starts')
    .given('This one is pending')
    .when('this is run it is pending too, despite returning true', () => true)
    .then('this is also pending despite returning false', () => false)
    .and('finally this is also pending despite returning false', () => false);
let scenario1 = workingFeature.scenario('A first feature can be created that uses scenario state');
scenario1
    .given('I have a first feature where I set a message', state => {
    state.message = 'Hello';
})
    .when('I update the text in the state property', state => {
    state.message += ' World!';
})
    .then('the value stored is updated', state => state.message === 'Hello World!');
scenario1
    .scenario('A child scenario of Scenario 1 can be created')
    .given('that I have created a child feature of Scenario 1', () => true)
    .when('the test runs', () => true)
    .then(`I can see that this scenario ran as part of it's parent, Scenario 1`, () => true);
workingFeature
    .scenario('I can pass initial state params and modify and use them', [{ name: 'Fred' }, { name: 'Bob' }])
    .given(`that the state is set to it's initial values`, people => people[0].name === 'Fred' && people[1].name === 'Bob')
    .when('I change the values', people => {
    people[0].name = 'Steve';
    people[1].name = 'Ferdinand';
})
    .then('they are successfully updated', people => people[0].name === 'Steve' && people[1].name === 'Ferdinand');
let failingFeature = _1.Feature.start('This is a broken feature');
failingFeature
    .scenario('This is the failing scenario')
    .given('that I have a feature that is broken', () => true)
    .when('when the failing scenario is tested', () => true)
    .then('it fails to pass the test', () => Promise.reject('Something is wrong with this scenario, check it out!'));
(() => __awaiter(this, void 0, void 0, function* () {
    yield workingFeature.run('all');
    yield failingFeature.run('all');
    yield assertFeature.run('all');
    yield pendingFeature.run('all');
    fs.writeFileSync(__filename + '.json', JSON.stringify([workingFeature, failingFeature, assertFeature, pendingFeature], null, 4), 'utf8');
    fs.writeFileSync(__filename + '.html', [yield workingFeature.runHtml(),
        yield failingFeature.runHtml(),
        yield assertFeature.runHtml(),
        yield pendingFeature.runHtml()].join('\n'), 'utf8');
    fs.writeFileSync(__dirname + '/readme.md', [yield workingFeature.runMarkdown(),
        yield failingFeature.runMarkdown(),
        yield assertFeature.runMarkdown(),
        yield pendingFeature.runMarkdown()].join('\n'), 'utf8');
    process.exit(0);
}))();
//# sourceMappingURL=Test.js.map